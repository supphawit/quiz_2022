import React, { useState } from "react";
import "../css/quiz1.css";

export const Quiz1 = () => {
  const [number, setnumber] = useState(0);
  const [calculation, setcalculation] = useState("prime");

  const test_prime = (n) => {
    if (n === 1) {
      return false;
    } else if (n === 2) {
      return true;
    } else {
      for (var x = 2; x < n; x++) {
        if (n % x === 0) {
          return false;
        }
      }
      return true;
    }
  };

  const test_fibonacci = (query, count = 1, last = 0) => {
    if (count < query) {
      return test_fibonacci(query, count + last, count);
    }
    if (count === query) {
      return true;
    }
    return false;
  };

  return (
    <div className="container">
      <div className="one">
        <p>
          <input
            type="number"
            value={number}
            onChange={(e) =>
              setnumber(
                parseFloat(e.target.value) >= 0 ? Math.round(e.target.value) : 1
              )
            }
          />
        </p>
      </div>
      <div className="two">
        <p>
          <select
            value={calculation}
            onChange={(e) => {
              setcalculation(e.target.value);
            }}
          >
            <option value="prime">isPrime</option>
            <option value="fibonacci">isFibonacci</option>
          </select>
        </p>
      </div>
      <div className="three">
        <p>
          {calculation === "prime"
            ? test_prime(number).toString()
            : test_fibonacci(number).toString()}
        </p>
      </div>
    </div>
  );
};
