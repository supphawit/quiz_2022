import React, { useEffect, useState } from "react";
import axios from "axios";

export const Quiz2 = () => {
  const [data, setdata] = useState(null);
  const [keyword, setkeyword] = useState("");

  const getData = async () => {
    let { data, status } = await axios.get(
      `https://api.publicapis.org/categories`
    );
    if (status === 200) setdata(data);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <table border="1" width="250">
        <thead>
          <tr>
            <th>
              <input
                value={keyword}
                onChange={(e) => {
                  setkeyword(e.target.value);
                }}
              />
            </th>
          </tr>
        </thead>
        <tbody>
          {keyword
            ? data &&
              data.categories
                .filter((e) =>
                  e.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())
                )
                .map((val, i) => (
                  <tr key={i}>
                    <td>{val}</td>
                  </tr>
                ))
            : data &&
              data.categories.map((val, i) => (
                <tr key={i}>
                  <td>{val}</td>
                </tr>
              ))}
        </tbody>
      </table>
    </div>
  );
};
