import { Switch, Route, Link } from "react-router-dom";
import "./App.css";
import { Quiz1 } from "./components/Quiz1";
import { Quiz2 } from "./components/Quiz2";

function App() {
  return (
    <div className="App">
      <button>
        <Link to="/quiz1">Quiz 1</Link>
      </button>{" "}
      <button>
        <Link to="/quiz2">Quiz 2</Link>
      </button>
      <br /> <br />
      <Switch>
        <Route path="/quiz1" component={Quiz1} />
        <Route path="/quiz2" component={Quiz2} />
      </Switch>
    </div>
  );
}

export default App;
